package com.englishgrammar.testapp.be.ui.learn

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.englishgrammar.testapp.be.R
import kotlinx.android.synthetic.main.fragment_grammar.*


class GrammarFragment : androidx.fragment.app.Fragment() {

    private var numberGrammar: Int = 0

    companion object {
        fun newInstance(numberGrammar: Int): GrammarFragment {
            val grammarFragment = GrammarFragment()
            grammarFragment.numberGrammar = numberGrammar
            return grammarFragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?):
            View? = inflater.inflate(R.layout.fragment_grammar, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        webView.settings.builtInZoomControls = true
        webView.loadUrl("file:///android_asset/" + (numberGrammar + 1) + ".html")
    }
}