package com.englishgrammar.testapp.be.ui.test

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import com.englishgrammar.testapp.be.R
import com.englishgrammar.testapp.be.ui.test.result.ResultUncompletedTestFragment
import kotlinx.android.synthetic.main.activity_test.*
import java.util.*

class TestActivity : AppCompatActivity() {

    private lateinit var testFragment: TestFragment
    private var catId: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_test)
        setSupportActionBar(toolbarPracticeTest)
        catId = intent.getIntExtra("catId", 0)
        testFragment = TestFragment.newInstance(catId)
        supportFragmentManager.beginTransaction().add(R.id.container, testFragment).commit()
    }

    companion object {
        var key = "VNbpHff6xlFtXQ"
    }

    override fun onBackPressed() {
        val returnIntent = Intent()
        setResult(Activity.RESULT_OK, returnIntent)
        super.onBackPressed()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_test, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> onBackPressed()
            R.id.question_list -> showResultUncompletedTest()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun showResultUncompletedTest() {
        if (testFragment.isVisible) {
            val subList = testFragment.resultTestList.filter { it.userAns != "" }
            Collections.sort(subList) { x, y -> x.getAnsResult().compareTo(y.getAnsResult()) }
            if (subList.isNotEmpty()) {
                val newFragment = ResultUncompletedTestFragment.newInstance(subList)
                supportFragmentManager.beginTransaction().replace(R.id.container, newFragment).addToBackStack("").commit()
            }
        }
    }
}