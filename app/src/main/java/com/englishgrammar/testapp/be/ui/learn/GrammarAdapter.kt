package com.englishgrammar.testapp.be.ui.learn

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.TextView
import com.englishgrammar.testapp.be.R
import com.minhphu.basemodule.ads.AdUtil
import kotlinx.android.synthetic.main.item_grammar.view.*


class GrammarAdapter(private val context: Context, private val grammarList: List<String>) : androidx.recyclerview.widget.RecyclerView.Adapter<GrammarAdapter.GrammarViewHolder>() {


    override fun getItemCount(): Int {
        return grammarList.size
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): GrammarViewHolder {
        val itemView = LayoutInflater.from(viewGroup.context).inflate(R.layout.item_grammar, viewGroup, false)
        val layoutNativeAds = itemView.findViewById<View>(R.id.adsContainer) as FrameLayout
        val ITEM_PER_ADS = 10
        if ((i + 5) % ITEM_PER_ADS == 0) {
            AdUtil.showNativeAdsFbSmallInList(context, layoutNativeAds, context.getString(R.string.adsFb_ListTestFragment), context.getString(R.string .adsGg_ListTestFragment))
        }
        return GrammarViewHolder(itemView)
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: GrammarViewHolder, position: Int) {
        holder.tvGrammar.text = (position + 1).toString() + ") " + grammarList[position]

    }

    inner class GrammarViewHolder(v: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(v) {
        var tvGrammar: TextView = v.tvGrammar

        init {
            tvGrammar.setOnClickListener {
                grammarEvent?.onItemCategoryClick(adapterPosition, grammarList[adapterPosition])
            }

        }
    }

    var grammarEvent: GrammarView? = null

    interface GrammarView {
        fun onItemCategoryClick(number_grammar: Int, title: String)
    }

}
