package com.englishgrammar.testapp.be.data

import com.vicpin.krealmextensions.query
import com.vicpin.krealmextensions.queryFirst
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

open class Category : RealmObject() {

    @PrimaryKey
    var id: Long = 0
    var subject: String? = null
    var name: String? = null
    var score: Long? = null

    fun getTotalTestNumber(): Int {
        return Test().query { equalTo("catId", id) }.count()
    }

    companion object {

        fun getAllBySubject(subject: String): List<Category> {
            return Category().query { equalTo("subject", subject) }
        }

        fun getCatById(cat_id: Int): Category? {
            return Category().queryFirst { equalTo("id", cat_id) }
        }
    }

}
