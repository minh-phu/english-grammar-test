package com.englishgrammar.testapp.be.ui.main

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.englishgrammar.testapp.be.R
import com.englishgrammar.testapp.be.data.Category
import com.englishgrammar.testapp.be.ui.main.MainActivity.Companion.COUNT_STORY_ADS
import com.englishgrammar.testapp.be.ui.main.MainActivity.Companion.LEVEL_1
import com.englishgrammar.testapp.be.ui.main.MainActivity.Companion.RESULT_SHOW_ADS
import com.englishgrammar.testapp.be.ui.test.TestActivity
import com.englishgrammar.testapp.be.utils.PreferenceUtils
import com.facebook.ads.Ad
import com.facebook.ads.AdError
import com.google.android.gms.ads.AdListener
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.InterstitialAd
import kotlinx.android.synthetic.main.fragment_category.*

class ListTestFragment : androidx.fragment.app.Fragment(), ListTestAdapter.ListCategoryView {
    private var subject: String = LEVEL_1
    private var cat: List<Category>? = null
    private var categoryAdapter: ListTestAdapter? = null

    companion object {
        fun newInstance(subject: String): ListTestFragment {
            val categoryFragment = ListTestFragment()
            categoryFragment.subject = subject
            return categoryFragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?)
            : View? = inflater.inflate(R.layout.fragment_category, container, false)


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initPopupFb()

        cat = Category.getAllBySubject(subject)
        categoryAdapter = ListTestAdapter(activity!!, cat!!)
        categoryAdapter?.categoryEvent = this
        recycler_view.layoutManager = androidx.recyclerview.widget.LinearLayoutManager(activity)
        recycler_view.adapter = categoryAdapter
    }

    override fun onItemCategoryClick(catId: Int) {
        val intent = Intent(activity, TestActivity::class.java)
        intent.putExtra("catId", catId)
        startActivityForResult(intent, RESULT_SHOW_ADS)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        (cat as ArrayList).clear()
        (cat as ArrayList<Category>).addAll(Category.getAllBySubject(subject))
        categoryAdapter?.notifyDataSetChanged()

        if (requestCode == RESULT_SHOW_ADS) {
            showAds++
            if (showAds >= COUNT_STORY_ADS) {
                if (!PreferenceUtils.checkPurchaseAd(requireContext())) {
                    try {
                        showInterstitialAds()
                    } catch (e: Exception) {

                    }
                }
            }
        }
    }


    private var mInterstitialAdGg: InterstitialAd? = null
    private var mInterstitialAdFb: com.facebook.ads.InterstitialAd? = null
    private var showAds = 0

    private fun initPopupAdGg() {
        if (activity != null) {
            mInterstitialAdGg = InterstitialAd(activity)
            mInterstitialAdGg?.adUnitId = getString(R.string.adsGg_Popup)
            if (!mInterstitialAdGg!!.isLoading && !mInterstitialAdGg!!.isLoaded) {
                val adRequest = AdRequest.Builder().build()
                mInterstitialAdGg?.loadAd(adRequest)
            }
            mInterstitialAdGg?.adListener = object : AdListener() {
                override fun onAdClosed() {
                    if (!mInterstitialAdGg!!.isLoading && !mInterstitialAdGg!!.isLoaded) {
                        val adRequest = AdRequest.Builder().build()
                        mInterstitialAdGg?.loadAd(adRequest)
                    }
                }
            }
        }
    }

    private fun initPopupFb() {
        if (activity != null) {
            mInterstitialAdFb = com.facebook.ads.InterstitialAd(activity, getString(R.string.adsFb_Popup))
            mInterstitialAdFb?.setAdListener(object : com.facebook.ads.InterstitialAdListener {
                override fun onAdClicked(p0: Ad?) {}

                override fun onAdLoaded(p0: Ad?) {}

                override fun onLoggingImpression(p0: Ad?) {}

                override fun onInterstitialDisplayed(ad: Ad) {}

                override fun onInterstitialDismissed(ad: Ad) {
                    mInterstitialAdFb!!.loadAd()
                }

                override fun onError(ad: Ad, adError: AdError) {
                    initPopupAdGg()
                }
            })
            mInterstitialAdFb?.loadAd()
        }
    }


    private fun showInterstitialAds() {
        if (mInterstitialAdFb != null && mInterstitialAdFb!!.isAdLoaded) {
            mInterstitialAdFb?.show()
            showAds = 0
        } else {
            if (mInterstitialAdGg != null && mInterstitialAdGg!!.isLoaded) {
                mInterstitialAdGg!!.show()
                showAds = 0
            }
        }
    }

    override fun onDestroy() {
        if (mInterstitialAdFb != null) {
            mInterstitialAdFb!!.destroy()
        }
        super.onDestroy()
    }
}
