package com.englishgrammar.testapp.be.ui.learn

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.englishgrammar.testapp.be.R
import com.englishgrammar.testapp.be.data.ModelGrammar
import kotlinx.android.synthetic.main.fragment_grammar_list.*

class GrammarListFragment : androidx.fragment.app.Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?):
            View? = inflater.inflate(R.layout.fragment_grammar_list, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val categoryAdapter = GrammarAdapter(activity!!, ModelGrammar.getGrammar())
        categoryAdapter.grammarEvent = activity as GrammarAdapter.GrammarView
        grammar_list_recycler_view.layoutManager = androidx.recyclerview.widget.LinearLayoutManager(activity)
        grammar_list_recycler_view.adapter = categoryAdapter
    }
}