package com.englishgrammar.testapp.be.data

class ResultTest {
    var question: String? = null
    var ans: String? = null
    var userAns: String? =null
    fun getAnsResult(): Boolean {
        return ans == userAns
    }
}