package com.englishgrammar.testapp.be.data;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Minh on 02/08/2016.
 */
public class ModelGrammar {
    public static List<String> getGrammar() {
        List<String> listGrammar = new ArrayList<>();
        listGrammar.add("Regular Verbs");
        listGrammar.add("Irregular Verbs");
        listGrammar.add("The Indefinite Article : a/an");
        listGrammar.add("The Definite Article : the");
        listGrammar.add("Prepositions of Place : In/At/On");
        listGrammar.add("Prepositions of Time : In/At/On");

        listGrammar.add("Present Simple");
        listGrammar.add("Present Continuous");
        listGrammar.add("Present Perfect");
        listGrammar.add("Present Perfect Continuous");

        listGrammar.add("Past Simple");
        listGrammar.add("Past Continuous");
        listGrammar.add("Past Perfect");
        listGrammar.add("Past Perfect Continuous");

        listGrammar.add("Future Simple");
        listGrammar.add("Future Continuous");
        listGrammar.add("Future Perfect");
        listGrammar.add("Future Perfect Continuous");

        listGrammar.add("Personal Pronouns");
        listGrammar.add("Possessive Adjectives");
        listGrammar.add("Possessive Pronouns");
        listGrammar.add("Reflexive Pronouns");
        return listGrammar;
    }
}
