package com.englishgrammar.testapp.be.ui.learn

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import android.view.MenuItem
import com.englishgrammar.testapp.be.R
import kotlinx.android.synthetic.main.activity_grammar.*


class GrammarActivity : AppCompatActivity(), GrammarAdapter.GrammarView {

    override fun onItemCategoryClick(number_grammar: Int, title: String) {
        setTitle(title)
        val transaction = supportFragmentManager.beginTransaction()
        val grammarFragment = GrammarFragment.newInstance(number_grammar)
        transaction.replace(R.id.mainContainer, grammarFragment).addToBackStack("")
        transaction.commit()
    }

    private fun setUpToolBar() {
        setSupportActionBar(toolbarGrammar)
        val actionBar = supportActionBar
        actionBar?.setDisplayHomeAsUpEnabled(true)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        when (id) {
            android.R.id.home -> onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_grammar)
        setUpToolBar()

        val transaction = supportFragmentManager.beginTransaction()
        val categoryFragment = GrammarListFragment()
        transaction.replace(R.id.mainContainer, categoryFragment)
        transaction.commit()
    }

}
