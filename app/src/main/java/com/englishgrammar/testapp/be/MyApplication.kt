package com.englishgrammar.testapp.be

import android.content.SharedPreferences
import android.preference.PreferenceManager
import androidx.multidex.MultiDexApplication
import com.crashlytics.android.Crashlytics
import com.englishgrammar.testapp.be.utils.Utils
import com.google.android.gms.ads.MobileAds
import com.google.firebase.remoteconfig.FirebaseRemoteConfig
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings
import io.fabric.sdk.android.Fabric
import io.realm.Realm
import io.realm.RealmConfiguration

class MyApplication : MultiDexApplication() {

    companion object {
        lateinit var sharedPreferences: SharedPreferences
    }

    override fun onCreate() {
        super.onCreate()
        Fabric.with(this, Crashlytics())
        MobileAds.initialize(applicationContext, getString(R.string.app_ads_id))
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(applicationContext)
        val key = sharedPreferences.getString("app_id", "")
        if (key!!.isEmpty() || key.length != 50) {
            val editor = sharedPreferences.edit()
            editor.putString("app_id", getString(R.string.app_id))
            editor.apply()
        }

        Realm.init(this)
        val config = RealmConfiguration.Builder().assetFile("assets.zip").build()
        Realm.setDefaultConfiguration(config)

        Utils.initKey(this)
        com.minhphu.basemodule.utils.CipherHelper.init(resources.getString(R.string.dev_url),Utils.key)
        setupRemoteConfig()
    }

    private fun setupRemoteConfig() {
        val remoteConfig = FirebaseRemoteConfig.getInstance()
        val configSettings = FirebaseRemoteConfigSettings.Builder()
                .setMinimumFetchIntervalInSeconds(3600L)
                .build()
        remoteConfig.setConfigSettingsAsync(configSettings)
        remoteConfig.fetchAndActivate()
    }
}
